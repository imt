let note_re = Str.regexp "^Note: including file:  *\\(.*\\)$"

let process_cl_dep7_output_line rejrel s set =
  if Str.string_match note_re s 0
  then
    let win_path = Utils.safe_group 1 s in
    let unix_path = Path.find2 win_path in
      match unix_path with
        | Some path ->
            if Utils.reject_path path rejrel
            then
              set
            else
              Utils.StringSet.add path set
        | None ->
            prerr_string
              "Dep7.process_cl_dep7_output_line: Cannot find unix path for "
            ;
            prerr_endline (String.escaped win_path);
            set
  else
    begin
      Cl.process_cl_output_line s;
      set
    end

let test () =
  let re = Str.regexp "-showIncludes" in
  let tool = Utils.tool_name "cl" in
  let command = Wine.command tool ^ " -?" in
  let ic = Unix.open_process_in command in
  let process s =
    if Str.string_match re s 0
    then
      exit 0
    else
      ()
  in
    Utils.iter_crlf_chan ic process;
    exit 1

let invoke_aux deponly oc target argv arg_start =
  let rejrel = Utils.build_reject_list () in
  let args = Utils.make_arg_string Path.check_and_modify_absolute argv arg_start in
  let tool = Utils.tool_name "cl" in
  let command =
    Utils.construct_args tool args
      (if deponly
        then
          "-Zs -showIncludes -nologo"
        else
          "-showIncludes -nologo"
      )
  in
  let ic = Unix.open_process_in command in
  let ifiles =
    Utils.fold_crlf_chan ic (process_cl_dep7_output_line rejrel)
      Utils.StringSet.empty
  in
  let code = Utils.close_process_in ic in
    if code = 0
    then
      begin
        Utils.output_quoted_path oc target;
        output_string oc ": ";
        Utils.StringSet.iter
          begin
            fun s ->
              Utils.output_quoted_path oc s;
              output_char oc ' '
          end ifiles;
        output_string oc "\n";
      end;
    code

let invoke = invoke_aux true stdout

let invoke2 target depfile argv arg_start =
  let oc =
    if depfile = "CON" || depfile = "con"
    then
      stdout
    else
      try
        open_out depfile
      with exn ->
        prerr_endline
          ("Dep7.invoke2 could not open output file `" ^ depfile ^ "':\n    " ^
              (Printexc.to_string exn));
        exit 101
  in
  let code = invoke_aux false oc target argv arg_start in
    close_out oc;
    code
