let debug = false

let line_re = Str.regexp "^#line [0-9]+ \"\\(.*\\)\"$"
let over_re = Str.regexp
    "^cl : Command line warning D4025 : overriding '/W[^']+' with '/w'"

let process_cl_cpp_output_line s set =
  if Str.string_match line_re s 0
  then
    let win_path = Utils.safe_group 1 s in
      if debug && (not (Utils.StringSet.mem win_path set))
      then
        prerr_endline s
      ;
      Utils.StringSet.add win_path set
  else
    set

let invoke target argv arg_start =
  let rejrel = Utils.build_reject_list () in
  let args = Utils.make_arg_string Path.check_and_modify_absolute argv arg_start in
  let tool = Utils.tool_name "cl" in
  let command = Utils.construct_args tool args "-E -nologo -w" in
  let env = Unix.environment () in
  let (ic, _, ec) as channels = Unix.open_process_full command env in
  let ifiles =
    Utils.fold_crlf_chan ic process_cl_cpp_output_line Utils.StringSet.empty in
    Utils.iter_crlf_chan ec
      begin
        fun s ->
          if not (Str.string_match over_re s 0)
          then
            prerr_endline s
      end;
    let code = Utils.close_process_full channels in
      if code = 0
      then
        begin
          Utils.output_quoted_path stdout target;
          print_string ": ";
          Utils.StringSet.iter
            begin
              fun win_path ->
                let unix_path = Path.find2 win_path in
                  match unix_path with
                    | None ->
                        prerr_string "Cannot convert windows path: '";
                        prerr_string (String.escaped win_path);
                        prerr_endline "' to unix"
                    | Some unix_path ->
                        if not (Utils.reject_path unix_path rejrel)
                        then
                          begin
                            Utils.output_quoted_path stdout unix_path;
                            print_char ' '
                          end
            end ifiles;
          print_newline ();
        end;
      code
