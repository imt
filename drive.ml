let drive_tbl = Array.create (Char.code 'z' - Char.code 'a' + 1) None

let root =
  let rec loop i =
    if i = Array.length drive_tbl
    then
      None
    else
      match drive_tbl.(i) with
        | Some "" ->
            Some (Char.chr (Char.code 'a' + i))
        | _ ->
            loop (succ i)
  in
     lazy (loop 0)

let get_root () =
  Lazy.force_val root

let subst s =
  let slen = String.length s in
    if slen >= 3
    then
      if s.[1] = ':'
      then
        if s.[2] = '/'
        then
          let c = Char.lowercase s.[0] in
(*             prerr_string "Drive "; *)
(*             prerr_char c; *)
(*             prerr_newline (); *)
          let i = Char.code c - Char.code 'a' in
            if i < Array.length drive_tbl
            then
              match drive_tbl.(i) with
                | None ->
                    s
                | Some d ->
                    let dlen = String.length d in
                    let r = String.create (slen - 2 + dlen) in
                      StringLabels.blit
                        ~src:d
                        ~src_pos:0
                        ~dst:r
                        ~dst_pos:0
                        ~len:dlen;
                      StringLabels.blit
                        ~src:s
                        ~src_pos:2
                        ~dst:r
                        ~dst_pos:dlen
                        ~len:(slen - 2);
                      r
            else
              begin
                prerr_string "Drive.subst bogus drive ";
                prerr_endline (String.escaped s);
                s
              end
        else
          begin
            prerr_string "Drive.subst no slash ";
            prerr_endline (String.escaped s);
            s
          end
      else
        s
    else
      s

let process_exn s = function
  | Unix.Unix_error(code, fn_name, fn_arg) ->
      prerr_string s;
      prerr_string ": ";
      prerr_string fn_name;
      prerr_char '(';
      if String.length fn_arg > 0
      then
        prerr_string fn_arg;
      prerr_string "): ";
      prerr_endline (Unix.error_message code)
  | exn ->
      prerr_string s;
      prerr_string ": ";
      prerr_endline (Printexc.to_string exn)

let process_dosdevices dir_path dir =
  let get_path_drive win_path =
    if String.length win_path = 2
    then
      if win_path.[1] = ':'
      then
        let c = Char.lowercase win_path.[0] in
          if (c >= 'a' && c <= 'z')
          then
            Some c
          else
            None
      else
        None
    else
      None
  in

  let put_drive drive_letter unix_path =
    let i = Char.code drive_letter - Char.code 'a' in
    let l = String.length unix_path in
    let drive_path =
      if unix_path.[l - 1] = '/'
      then
        String.sub unix_path 0 (pred l)
      else
        unix_path
    in
      drive_tbl.(i) <- Some drive_path
  in

  let rec loop () =
    let opt_s =
      try
        let s = Unix.readdir dir in
          Some s
      with
        | End_of_file ->
            (* prerr_endline "Drive(readdir): empty directory"; *)
            None
    in
      match opt_s with
        | None ->
            ()
        | Some s ->
            begin
              match get_path_drive s with
                | Some c ->
                    let opt_target =
                      try
                        Some (Unix.readlink (Filename.concat dir_path s))
                      with
                        | exn ->
                            process_exn
                              "Drive.process_dosdevices(loop:readlink)"
                              exn;
                            None
                    in
                      Utils.some_action (put_drive c) () opt_target

                | None ->
                    ()
            end;
            loop ()
  in
    try
      loop ()
    with
      | exn ->
          process_exn "Drive.process_dosdevices(loop:?)" exn

let init () =
  if Wine.native
  then
    let _A = Char.code 'A' in
      for i = 0 to pred (Array.length drive_tbl) do
        let s = "_:" in
          s.[0] <- Char.chr (i + _A);
          drive_tbl.(i) <- Some s
      done
  else
    let dir_path = Filename.concat Wine.root_path "dosdevices" in
    let opt_dir =
      try
        Some (Unix.opendir dir_path)
      with
        | exn ->
            process_exn "Drive.init" exn;
            None
    in
      match opt_dir with
        | None ->
            prerr_endline "Can not establish drive mapping";
        | Some dir ->
            process_dosdevices dir_path dir;
            begin
              try
                Unix.closedir dir
              with
                | exn ->
                    process_exn "Drive.init" exn;
            end
