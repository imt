let native_convert_slashes = ref false

let native = Sys.os_type = "Win32"

let wine_command s = "wine " ^ s
let native_command s = s

let command =
  if native
  then
    native_command
  else
    wine_command

let root_path =
  if native
  then
    "/"
  else
    try
      Sys.getenv "WINE_PREFIX"
    with
      | Not_found ->
          begin
            try
              Filename.concat (Sys.getenv "HOME") ".wine"
            with
              | Not_found ->
                  failwith "Can not find wine's root path"
          end
