let help () =
  print_string "usage: ";
  print_string (Filename.basename Sys.executable_name);
  print_endline " command [ command-argument ] [ arguments ... ]";
  print_endline "command is one of: ";
  List.iter (fun s -> print_string "    "; print_endline s)
    ["cl";
     "  run `cl [arguments]' sanitizing output (converting paths etc)";
     "";

     "dep target";
     "  run `cl /E [arguments]'";
     "  search output for headers";
     "  print `target: <list-of-headers>' to standard output";
     "";

     "dep7 target";
     "  run `cl /showIncludes /Zs [arguments]'";
     "  same as dep";
     "";

     "dep7-test";
     "  test if cl is new enough and has /showIncludes option";
     "";

     "cldep7 target depfile";
     "  run `cl /showIncludes [arguments]'";
     "  same as cl+dep7 but put the dependency information into file <depfile>";
     "       (sans /Zs)";
     "";

     "link";
     "  run `link [arguments]'";
     "";

     "help";
     "  show this help screen";
     ""
    ];
  let exp_cmd, run_cmd =
    if Wine.native
    then
      "C:\\> set ", "C:\\> "
    else
      "$ export ", "$ "
  in
  List.iter print_endline
    ["If environment variable IMT_[CL|LINK] is set it will be used as the ";
     "path to respective utility, otherwise [cl.exe|link.exe] will be used";
     "";
     "If environment variable IMT_VIA is set it will be used as the path ";
     "to the application to run instead of [cl.exe|link.exe]";
     "IMT_VIA_PASS_NAME environment variable can be used to specify the name";
     "of the corresponding utility down the chain";
     "IMT_VIA_PASS_ARGS can be used to pass parameters of the utility";
     "";
     "Headers produced by [cl]dep[7] commands will be tested against IMT_REJ";
     "environment variable (: separated list of OCaml style regular expressions)";
     "if the path to header matches it will be excluded from the printout";
     "(paths are tested after windows to unix translation (nop on Win platforms))";
     "";
     "Example:";
     exp_cmd ^ "IMT_REJ=/mnt/windows/msc/include:/mnt/psdk/include";
     exp_cmd ^ "IMT_CL=/mnt/windows/msc/bin/cl.exe";
     run_cmd ^ "imt cldep7 moo.obj moo.dep -c moo.c";
     "";
     exp_cmd ^ "IMT_VIA=ocamlc";
     exp_cmd ^ "IMT_VIA_PASS_NAME=-cc";
     exp_cmd ^ "IMT_VIA_PASS_ARGS=-ccopt";
     run_cmd ^ "imt cl -c moo.ml";
     "will result in execution of `ocamlc -cc \"cl\" -ccopt \"-nologo\" moo.ml'";
     ""
    ];
  print_endline "Have a nice day";
  exit 0

let show_usage () =
  print_endline "Incredible Mega Thing by extremely cool hacker, Version 1.01";
  print_newline ();
  print_string "usage: ";
  print_string (Filename.basename Sys.executable_name);
  print_endline " command [ command-argument ] [ arguments ... ]";
  print_endline "command is one of: ";
  List.iter (fun s -> print_string "    "; print_endline s)
    ["cl: invoke cl";
     "dep target: invoke cl /E, parse output, print dependencies to stdout";
     "dep7 target: invoke cl /showIncludes /Zs, same as dep";
     "dep7-test: test if /showIncludes is available, exit with non-zero if not";
     "cldep7 target depfile: cl+dep7 combined, put dependencies into depfile";
     "link: invoke link";
     "help: show extended help"
    ];
  print_endline "Have a nice day";
  exit 100

let main argv =
  if Array.length argv < 2
  then
    show_usage ()
  else
    begin
      Drive.init ();
      let code =
        match argv.(1) with
          | "cl" ->
              Cl.invoke argv 2

          | "dep7" ->
              if Array.length argv < 3
              then
                show_usage ()
              else
                let target = argv.(2) in
                  Dep7.invoke target argv 3

          | "dep" ->
              if Array.length argv < 3
              then
                show_usage ()
              else
                let target = argv.(2) in
                  Dep.invoke target argv 3

          | "link" ->
              Link.invoke argv 2

          | "dep7-test" ->
              Dep7.test ()

          | "cldep7" ->
              if Array.length argv < 4
              then
                show_usage ()
              else
                let target = argv.(2)
                and depfile = argv.(3) in
                  Dep7.invoke2 target depfile argv 4

          | "help" ->
              help ()

          | cmd ->
              prerr_string "invalid command: ";
              prerr_string (String.escaped cmd);
              prerr_newline ();
              show_usage ()
      in
        exit code
    end

let _ =
  main Sys.argv
