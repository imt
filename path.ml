let debug = false

type unix_path =
  | Bad_dir
  | Bad_file
  | Path of string

let convert_compress_slashes s =
  let l = String.length s in
  let t = String.create l in
  let rec loop prev_slash i j =
    if i = l
    then
      j
    else
      let c = s.[i] in
        if c = '\\'
        then
          if prev_slash
          then
            loop true (succ i) j
          else
            begin
              t.[j] <- '/';
              loop true (succ i) (succ j)
            end
        else
          begin
            t.[j] <- c;
            loop false (succ i) (succ j)
          end
  in
  let rl = loop false 0 0 in
    if rl = String.length t
    then
      t
    else
      String.sub t 0 rl

let convert_slashes = convert_compress_slashes

let find_unix_path path =
  let dirname = Filename.dirname path in
    if debug
    then
      Format.eprintf "find_unix_path: %S - %S %b@.@."
        path dirname (Sys.file_exists dirname);
    if not (Sys.file_exists dirname)
    then
      Bad_dir
    else
      if not (Sys.file_exists path)
      then
        let basename = Filename.basename path in
        let names = Sys.readdir dirname in
          if debug
          then
            Format.eprintf "readdir: %S@." dirname;
        let lowercase_name = String.lowercase basename in
        let find_string_lower target names =
          let len = Array.length names in
          let rec finder index =
            if index = len
            then
              None
            else
              let name = names.(index) in
              let lowercase_name = String.lowercase name in
                if lowercase_name = target
                then
                  Some name
                else
                  finder (succ index)
          in
            finder 0
        in
          match (find_string_lower lowercase_name names) with
            | None ->
                Bad_file
            | Some name ->
                Path (Filename.concat dirname name)
      else
        Path path

let rec find path =
  if debug
  then
    Format.eprintf "find(%S)@." path;
  let res = find_unix_path path in
    match res with
      | Bad_dir ->
          let dirname = Filename.dirname path in
          let res = find dirname in
            begin
              match res with
                | Path correct_dirname ->
                    let basename = Filename.basename path in
                    let path = Filename.concat correct_dirname basename in
                      find_unix_path path
                | _ ->
                    Bad_dir
            end
      | other ->
          other

let split s =
  let len = String.length s in
  let rec collect list pos =
    if pos = len
    then
      list
    else
      match s.[pos] with
        | ' ' | '\t' | '\n' | '\r' ->
            collect list (succ pos)
        | '"' ->
            let endpos =
              try
                String.index_from s pos '"'
              with Not_found->
                prerr_string "Path.split: mismatched quote ";
                prerr_string (String.escaped s);
                prerr_string " pos:";
                prerr_int pos;
                prerr_newline ();
                len
            in
            let s = String.sub s pos (endpos - pos) in
              collect (s :: list) endpos
        | _ ->
            let endpos =
              try
                String.index_from s pos ' '
              with Not_found->
                len
            in
            let s = String.sub s pos (endpos - pos) in
              collect (s :: list) endpos
  in
    collect [] 0

let find2 win_path =
  let s1 =
    if not Wine.native || !Wine.native_convert_slashes
    then
      convert_slashes win_path
    else
      win_path
  in
    if Wine.native
    then
      Some s1
    else
      let s2 = Drive.subst s1 in
      let unix_path = find s2 in
        begin
          match unix_path with
            | Bad_dir
            | Bad_file ->
                if debug
                then
                  begin
                    prerr_string "Path.find2 Bad_file: ";
                    prerr_string (String.copy win_path);
                    prerr_char ' ';
                    prerr_string (String.copy s1);
                    prerr_char ' ';
                    prerr_string (String.copy s2);
                    prerr_char ' ';
                    prerr_newline ();
                    exit 103
                  end;
                None
            | Path unix_path ->
                Some unix_path
        end

let output oc dir_only win_path =
  let s1 =
    if not Wine.native || !Wine.native_convert_slashes
    then
      convert_slashes win_path
    else
      win_path
  in
    if Wine.native
    then
      output_string oc s1
    else
      let s2 = Drive.subst s1 in
      let s3 =
        if dir_only
        then
          Filename.dirname s2
        else
          s2
      in
      let unix_path = find s3 in
        begin
          match unix_path with
            | Bad_dir
            | Bad_file ->
                if debug
                then
                  begin
                    prerr_string "Path.output Bad_file: ";
                    prerr_string ( win_path);
                    print_char ' ';
                    prerr_string ( s1);
                    prerr_char ' ';
                    prerr_string ( s2);
                    prerr_char ' ';
                    prerr_newline ();
                    exit 104
                  end;
                output_string oc win_path
            | Path unix_path ->
                if dir_only
                then
                  output_string oc (Filename.concat s3 (Filename.basename s2))
                else
                  output_string oc unix_path
        end

let prerr = output stderr
let print = output stdout

let abs_predicate s =
  not (Filename.is_relative s) && Sys.file_exists s

let check_and_modify_absolute_no_root s =
  if abs_predicate s
  then
    begin
      prerr_string (String.escaped s);
      prerr_endline
        " is absolute and exists, but no wine drive mapping root is defined";
      s
    end
  else
    s

let check_and_modify_absolute_root root s =
  if abs_predicate s
  then
    if false
    then
      let r = String.create (String.length s + 2) in
        r.[0] <- root;
        r.[1] <- ':';
        StringLabels.blit
          ~src:s
          ~src_pos:0
          ~dst:r
          ~dst_pos:2
          ~len:(String.length s);
        r
    else
      let r = String.create (String.length s + 1) in
        r.[0] <- '\\';
        r.[1] <- '\\';
        StringLabels.blit
          ~src:s
          ~src_pos:1
          ~dst:r
          ~dst_pos:2
          ~len:(String.length s - 1);
        r
  else
    s

let check_and_modify_absolute s =
  if Wine.native
  then
    s
  else
    match Drive.get_root () with
      | None ->
          check_and_modify_absolute_no_root s
      | Some root ->
          check_and_modify_absolute_root root s
