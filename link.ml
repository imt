let emit_endline = ref prerr_endline

let process_link_output s =
  if s <> "LINK : warning LNK4044: unrecognized option '/nologo'; ignored"
  then
    !emit_endline s

let invoke argv arg_start =
  let is_empty s =
    let rec loop i =
      if i = String.length s
      then
        true
      else
        if s.[i] = ' ' || s.[i] = '\t'
        then
          loop (succ i)
        else
          false
    in
      loop 0
  in

  let _ =
    if Utils.array_find_from
      (fun s -> s = "/dump" || s = "-dump" || Utils.question_pred s)
      argv arg_start
    then
      emit_endline := print_endline
  in

  let args = Utils.make_arg_string Path.check_and_modify_absolute argv arg_start in
  let tool = Utils.tool_name "link" in
  let command =
    if is_empty args
    then
      Utils.construct_args tool "" ""
    else
      Utils.construct_args tool args "-nologo"
  in
  let ic = Unix.open_process_in command in
    Utils.iter_crlf_chan ic process_link_output;
    let code = Utils.close_process_in ic in
      code
