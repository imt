module StringSet = Set.Make (struct type t = string let compare = compare end)

let decrlf line =
  if line.[String.length line - 1] = '\r'
  then
    String.sub line 0 (String.length line - 1)
  else
    line

let safe_input_line ic =
  try
    Some (input_line ic)
  with
    | End_of_file ->
        None

let iter_crlf_chan ic f =
  let rec loop () =
    let opt_line = safe_input_line ic in
      match opt_line with
        | Some line ->
            f (decrlf line);
            loop ()
        | None ->
            ()
  in
    loop ()

let fold_crlf_chan ic f init =
  let rec loop accu =
    let opt_line = safe_input_line ic in
      match opt_line with
        | Some line ->
            let accu = f (decrlf line) accu in
              loop accu
        | None ->
            accu
  in
    loop init

let process_status = function
  | Unix.WEXITED code ->
      code
  | Unix.WSIGNALED signum ->
      prerr_string "signalled ";
      prerr_int signum;
      prerr_newline ();
      101
  | Unix.WSTOPPED signum ->
      prerr_string "stopped ";
      prerr_int signum;
      prerr_newline ();
      102

let close_process_in ic =
  let status = Unix.close_process_in ic in
    process_status status

let close_process_full channels =
  let status = Unix.close_process_full channels in
    process_status status

let make_arg_string f argv pos =
  let rec loop accu i =
    if i >= Array.length argv
    then
      List.rev accu
    else
      let s = f argv.(i) in
      let accu = s :: accu in
        loop accu (succ i)
  in
  let arg_list = loop [] pos in
    String.concat " " arg_list

let safe_group_extents group_nr =
  try
    Str.group_beginning group_nr, Str.group_end group_nr
  with Not_found ->
    failwith "Internal error (cannot get group extents)"

let safe_group group_nr s =
  try
    Str.matched_group group_nr s
  with Not_found ->
    failwith "Internal error (cannot find matched group)"

let safe_group_end group_nr =
  try
    Str.group_end group_nr
  with Not_found ->
    failwith "Internal error (cannot find group end)"

let some_action f d = function
  | None ->
      d
  | Some v ->
      f v

let array_find_from p a pos =
  let rec loop i =
    if i >= Array.length a
    then
      false
    else
      p a.(i) || loop (succ i)
  in
    loop pos

let question_pred s = s = "/?" || s = "-?"
let contains_question argv arg_start =
  array_find_from question_pred argv arg_start

let tool_name s =
  try
    Unix.getenv ("IMT_" ^ (String.uppercase s))
  with Not_found ->
    s

let build_reject_list () =
  let rejs =
    try
      Some (Unix.getenv "IMT_REJ")
    with Not_found ->
      None
  in
    match rejs with
      | None ->
          []

      | Some rejs ->
          let l = String.length rejs in
          let rec collect accu old_pos =
            if old_pos >= l
            then
              accu
            else
              let pos =
                try
                  String.index_from rejs old_pos ':'
                with Not_found->
                  l
              in
              let s = String.sub rejs old_pos (pos - old_pos) in
              let accu =
                let re =
                  if Sys.os_type = "Win32"
                  then
                    Str.regexp_string_case_fold s
                  else
                    Str.regexp_string s
                in
                  re :: accu
              in
                collect accu (pos + 1)
          in
            collect [] 0

let quotere = Str.regexp "\""

let quote s =
  if String.contains s ' '
  then
    let ss = Str.global_substitute quotere (fun _ -> "\\\"") s in
      "\"" ^ ss ^ "\""
  else
    s

let output_quoted_path oc s =
  if String.contains s ' '
  then
    let ss = Str.global_substitute quotere (fun _ -> "\\\"") s in
      output_char oc '"';
      output_string oc ss;
      output_char oc '"'
  else
    output_string oc s

let reject_path s = function
  | [] -> false
  | rejrel ->
      let rec find = function
        | [] -> false
        | rejre :: tl ->
            if Str.string_partial_match rejre s 0
            then
              true
            else
              find tl
      in
        find rejrel

let construct_args tool args extra =
  let cmdline =
    try
      let via = Sys.getenv "IMT_VIA" in
      let s1 =
        try
          let s = Sys.getenv "IMT_VIA_PASS_NAME" in
            s ^ " " ^ quote tool
        with Not_found ->
          tool
      and s2 =
        try
          let s = Sys.getenv "IMT_VIA_PASS_ARGS" in
            args ^ " " ^ s ^ " " ^ quote extra
        with Not_found ->
          args ^ " " ^ extra
      in
        via ^ " " ^ s1 ^ " " ^ s2
    with Not_found ->
      tool ^ " " ^ args ^ " " ^ extra
  in
    Wine.command cmdline

(* let construct_args tool args extra = *)
(*   let s = construct_args tool args extra in *)
(*     prerr_endline tool; *)
(*     prerr_endline args; *)
(*     prerr_endline extra; *)
(*     prerr_endline s; *)
(*     s *)
