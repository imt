let debug = false
let src_file_re = Str.regexp "^\\([ \t]*\\)\\(.*\\)([0-9]+) :"
let inc_file_re = Str.regexp "Cannot open include file: '\\(.*\\)'"

let process_include_err s =
  let rec loop pos =
    let opt_pos2 =
      try
        Some (Str.search_forward inc_file_re s pos)
      with Not_found ->
        None
    in
      match opt_pos2 with
        | Some pos2 ->
            let b, e = Utils.safe_group_extents 1 in
            let win_path = String.sub s b (e - b) in
              prerr_string (String.sub s pos b);
              Path.prerr true win_path;
              prerr_string (String.sub s e (String.length s - e));
              loop e
        | None ->
            prerr_string (String.sub s pos (String.length s - pos))
  in
    loop 0

let process_cl_output_line s =
  (* prerr_endline s; *)
  if Str.string_match src_file_re s 0
  then
    let group1 = Utils.safe_group 1 s in
    let win_path = Utils.safe_group 2 s in
      prerr_string group1;
      Path.prerr false win_path;
      let gend = Utils.safe_group_end 2 in
        process_include_err (String.sub s gend (String.length s - gend));
        prerr_newline ()
  else
    begin
      process_include_err s;
      prerr_newline ()
    end

let invoke argv arg_start =
  let tool = Utils.tool_name "cl" in
  let args = Utils.make_arg_string Path.check_and_modify_absolute argv arg_start in
    if Utils.contains_question argv arg_start
    then
      let command = Utils.construct_args tool args "" in
      let ic = Unix.open_process_in command in
        Utils.iter_crlf_chan ic print_endline;
        let code = Utils.close_process_in ic in
          code
    else
      let command = Utils.construct_args tool args "-nologo" in
      let ic = Unix.open_process_in command in
        Utils.iter_crlf_chan ic process_cl_output_line;
        let code = Utils.close_process_in ic in
          code
